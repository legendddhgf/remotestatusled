# RemoteStatusLED

a simple LED status indicator that can be updated remotely. For example, informing people who visit your desk where you are, without you needing to be there to update the LED.

Based on Adafruit 32x16 RGB LED board and AtMega2560