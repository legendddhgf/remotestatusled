#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "ledlib.h"
#include "wordmatrix.h"

int main(void){

  sei();

  led_init_pins();

  /*
  for (int x = 0; x < NUM_COLS / 2; x++) {
    for (int y = 0; y < NUM_ROWS / 2; y++) {
      led_add_color_for_pos(x, y, LED_BLUE);
    }
    for (int y = NUM_ROWS / 2; y < NUM_ROWS; y++) {
      led_add_color_for_pos(x, y, LED_GREEN);
    }
  }
  for (int x = NUM_COLS / 2; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS / 2; y++) {
      led_add_color_for_pos(x, y, LED_BLUE);
      led_add_color_for_pos(x, y, LED_RED);
    }
    for (int y = NUM_ROWS / 2; y < NUM_ROWS; y++) {
      led_add_color_for_pos(x, y, LED_GREEN);
      led_add_color_for_pos(x, y, LED_RED);
    }
  }



  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      led_set_color_for_pos(x, y, LED_RED | LED_BLUE);
    }
  }

  */


  int letter_xpos = 1;
  write_char_to_matrix('a', letter_xpos, 1, LED_GREEN, LED_NOCOLOR);
  letter_xpos += 6;
  write_char_to_matrix('4', letter_xpos, 1, LED_GREEN, LED_NOCOLOR);
  letter_xpos += 6;
  write_char_to_matrix('2', letter_xpos, 1, LED_GREEN, LED_NOCOLOR);
  letter_xpos += 6;
  write_char_to_matrix('0', letter_xpos, 1, LED_GREEN, LED_NOCOLOR);
  letter_xpos += 6;

  led_enable_autorun();

  int init_char_add = 0;
  int init_num_add = 0;
  while(1) {
    _delay_ms(500);
    write_char_to_matrix('a' + (char) init_char_add, 1, 1, LED_GREEN,
        LED_NOCOLOR);
    write_char_to_matrix('0' + (char) init_num_add, letter_xpos, 1, LED_BLUE,
        LED_NOCOLOR);
    init_char_add = (init_char_add + 1) % 26; // wrap around
    init_num_add = (init_num_add + 1) % 10; // wrap around
  }

  return 0;
}
