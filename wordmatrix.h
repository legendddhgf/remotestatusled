#ifndef WORDMATRIX_H
#define WORDMATRIX_H

#include "ledlib.h"

void write_char_to_matrix(char chartoconvert, uint32_t xpos, uint8_t ypos,
    uint8_t rgb, uint8_t rgb_bg);

#endif
