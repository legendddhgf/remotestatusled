#include "wordmatrix.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// xpos, ypos refer to upper left of where character whould be placed in matrix
// chartoconvert is what you want to write, LED_ARRAY is passed in to be
// written, and rgb/rgb_bg is simply the color for the character/background
// respectively
// NOTE: All characters are required to take up 5x5 on the matrix
// Thus, it's assumed you add 6 to the x index in the calling function
void write_char_to_matrix(char chartoconvert, uint32_t xpos, uint8_t ypos,
    uint8_t rgb, uint8_t rgb_bg) {
  // draw 5x5 background space starting from (xpos,ypos) top left corner
  for (uint8_t yiter = ypos; yiter < ypos + 5; yiter++) {
    for (uint8_t xiter = xpos; xiter < xpos + 5; xiter++) {
      led_set_color_for_pos(xiter, yiter, rgb_bg);
    }
  }
  if (islower(chartoconvert)) chartoconvert = toupper(chartoconvert);
  switch(chartoconvert) {
    case 'A':
      led_set_color_for_pos(xpos + 1, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'B':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case 'C':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'D':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      break;
    case 'E':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'F':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      break;
    case 'G':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'H':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'I':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'J':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      break;
    case 'K':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'L':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'M':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'N':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'O':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'P':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      break;
    case 'Q':
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'R':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'S':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'T':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      break;
    case 'U':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case 'V':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      break;
    case 'W':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case 'X':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case 'Y':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      break;
    case 'Z':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '0':
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case '1':
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '2':
      led_set_color_for_pos(xpos + 1, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '3':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case '4':
      led_set_color_for_pos(xpos + 0, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 0, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 0, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '5':
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '6':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    case '7':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      break;
    case '8':
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      break;
    case '9':
      led_set_color_for_pos(xpos, ypos, rgb);
      led_set_color_for_pos(xpos + 1, ypos, rgb);
      led_set_color_for_pos(xpos + 2, ypos, rgb);
      led_set_color_for_pos(xpos + 3, ypos, rgb);
      led_set_color_for_pos(xpos + 4, ypos, rgb);
      led_set_color_for_pos(xpos, ypos + 1, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 1, rgb);
      led_set_color_for_pos(xpos, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 2, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 3, rgb);
      led_set_color_for_pos(xpos, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 1, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 2, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 3, ypos + 4, rgb);
      led_set_color_for_pos(xpos + 4, ypos + 4, rgb);
      break;
    default:
      //ERROR, character not available for conversion
      break;
  }
}

/* Character LED mappings

// english letters

 XXX
X   X
XXXXX
X   X
X   X

XXXX
X   X
XXXX
X   X
XXXX

XXXXX
X
X
X
XXXXX

XXX
X  X
X   X
X  X
XXX

XXXXX
X
XXXXX
X
XXXXX

XXXXX
X
XXXXX
X
X

XXXXX
X
X XXX
X   X
XXXXX

X   X
X   X
XXXXX
X   X
X   X

XXXXX
  X
  X
  X
XXXXX

XXXXX
  X
  X
X X
XXX

X   X
X  X
XXX
X  X
X   X

X
X
X
X
XXXXX

X   X
XX XX
X X X
X   X
X   X

X   X
XX  X
X X X
X  XX
X   X

XXXXX
X   X
X   X
X   X
XXXXX

XXXX
X   X
XXXX
X
X

 XXX
X   X
X X X
X  X
 XX X

XXXXX
X   X
XXXX
X  X
X   X

XXXXX
X
XXXXX
    X
XXXXX

XXXXX
  X
  X
  X
  X

X   X
X   X
X   X
X   X
 XXX

X   X
X   X
X   X
 X X
  X

X   X
X   X
X X X
X X X
 X X

X   X
 X X
  X
 X X
X   X

X   X
 X X
  X
  X
  X

XXXXX
   X
  X
 X
XXXXX

// NUMBERS

 XXX
X   X
X   X
X   X
 XXX

  X
 XX
X X
  X
XXXXX

 XX
X  X
  X
 X
XX XX

XXXX
    X
 XXX
    X
XXXX

X   X
X   X
XXXXX
    X
    X

 XXXX
X
XXXXX
    X
XXXXX

XXXXX
X
XXXXX
X   X
XXXXX

XXXXX
   X
  X
 X
X

 XXX
X   X
 XXX
X   X
 XXX

XXXXX
X   X
XXXXX
    X
XXXXX

*/
