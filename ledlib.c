#include "ledlib.h"

#ifndef F_CPU
#define F_CPU 16e6
#endif

// index x,y = y * 32 + x
// orient board so (0,0) starts from top left corner and goes down/to the right
uint8_t LED_ARRAY[NUM_COLS * NUM_ROWS] = {0};

uint8_t led_global_enable = 0; // 0: don't draw, 1: draw

//LEDREGNAME example "LEDLAT"
#define _SETDDR(LEDREGNAME) (LEDREGNAME##_DDR |= LEDREGNAME##_BITMSK)
#define SETDDR(LEDREGNAME) (_SETDDR(LEDREGNAME))
#define _CLRDDR(LEDREGNAME) (LEDREGNAME##_DDR &= ~LEDREGNAME##_BITMSK)
#define CLRDDR(LEDREGNAME) (_CLRDDR(LEDREGNAME))
#define _SETPORT(LEDREGNAME) (LEDREGNAME##_PORT |= LEDREGNAME##_BITMSK)
#define SETPORT(LEDREGNAME) (_SETPORT(LEDREGNAME))
#define _CLRPORT(LEDREGNAME) (LEDREGNAME##_PORT &= ~LEDREGNAME##_BITMSK)
#define CLRPORT(LEDREGNAME) (_CLRPORT(LEDREGNAME))

ISR(TIMER1_COMPA_vect) {
  // don't allow frame drawing to be interrupted midway through
  unsigned char sreg = SREG;
  cli();
  led_draw_frame();
  TIFR1 &= ~_BV(OCF1A); // clear the flag that triggers the interrupt (note:
                        // this is not the same as disable the interrupt)
  SREG = sreg;
}

void led_init_pins() {
  SETDDR(LEDR1);
  SETDDR(LEDB1);
  SETDDR(LEDG1);
  SETDDR(LEDR2);
  SETDDR(LEDB2);
  SETDDR(LEDG2);
  SETDDR(LEDA);
  SETDDR(LEDB);
  SETDDR(LEDC);

  // and initially clear them all
  CLRPORT(LEDR1);
  CLRPORT(LEDB1);
  CLRPORT(LEDG1);
  CLRPORT(LEDR2);
  CLRPORT(LEDB2);
  CLRPORT(LEDG2);
  CLRPORT(LEDA);
  CLRPORT(LEDB);
  CLRPORT(LEDC);

  // These ones should explicitly be set to 0 initially, everything else we do
  SETDDR(LEDCLK);
  CLRPORT(LEDCLK);
  SETDDR(LEDLAT);
  CLRPORT(LEDLAT);
  SETDDR(LEDOE);
  //initially disable LED
  SETPORT(LEDOE);

  unsigned char sreg = SREG;
  cli(); // save and disable interrupts (according to datasheet, some of these
         // can require multiple access and can be corrupted if stopped partway)

  // init related interrupts::
  // We use CTC mode (start at bottom and go to OCR1A, then immediately wrap to
  // bottom again) which requires the following. WGM1[1:0] = 0b10
  TCCR1A |= _BV(WGM11);
  // F_CPU as clk/256: CS1[2:0] = 0b100
  TCCR1B |= _BV(CS12);
  OCR1A = (F_CPU / 1e6) - 1; // seemed like about the sweet spot
  TIMSK1 |= _BV(OCIE1A); // trigger interrupt when counter reaches OCR1A;

  SREG = sreg; // re-set interrupt status
}

// state a particular value for all of [RGB][01]
// // this function simply sets and clears CLK
void led_cycle_clk() {
  SETPORT(LEDCLK);
  CLRPORT(LEDCLK);
}

// state that we finished defining this row
// this function simply sets and clears LAT
void led_cycle_lat() {
  SETPORT(LEDLAT);
  CLRPORT(LEDLAT);
}

// essentially force all LEDs off instantly
// this function simply sets and clears OE
void led_cycle_oe() {
  SETPORT(LEDOE);
  CLRPORT(LEDOE);
}

// r: bit 2, g: bit 1, b: bit 0
void led_set_color_for_pos(uint8_t x, uint8_t y, uint8_t rgb) {
  // subtract one from index pos to re-offset entire array
  // these two corner cases need to have their vertical positions swapped
  y++; // offset needed
  if (y == 0) y = 8;
  else if (y == 8) y = 0;
  uint32_t array_pos = ((y + NUM_ROWS-1) % NUM_ROWS) * NUM_COLS + (x % NUM_COLS);
  LED_ARRAY[array_pos] = (rgb & 0x7);
}

// r: bit 2, g: bit 1, b: bit 0
void led_add_color_for_pos(uint8_t x, uint8_t y, uint8_t rgb) {
  // subtract one from index pos to re-offset entire array
  // these two corner cases need to have their vertical positions swapped
  if (y == 0) y = 8;
  else if (y == 8) y = 0;
  uint32_t array_pos = ((y + NUM_ROWS-1) % NUM_ROWS) * NUM_COLS + (x % NUM_COLS);
  LED_ARRAY[array_pos] |= (rgb & 0x7);
}

void led_draw_frame() {
  // new optimization: lighting 2 rows at a time with half the row changes
  for (int y = 0; y < NUM_ROWS / 2; y++) {
    if (y & 0x4) CLRPORT(LEDC);
    else SETPORT(LEDC);
    if (y & 0x2) CLRPORT(LEDB);
    else SETPORT(LEDB);
    if (y & 0x1) CLRPORT(LEDA);
    else SETPORT(LEDA);

    // force clear before starting new row
    led_cycle_oe(); // pwm done in calling instance
    // x iterated backwards to make it start from top left
    for (int x = NUM_COLS; x >= 0; x--) {
      if (led_global_enable != 1) {
        CLRPORT(LEDR1);
        CLRPORT(LEDG1);
        CLRPORT(LEDB1);
        CLRPORT(LEDR2);
        CLRPORT(LEDG2);
        CLRPORT(LEDB2);
        led_cycle_clk();
        continue;
      }
      uint32_t array_sz = NUM_COLS * NUM_ROWS;
      uint32_t array_pos = y * NUM_COLS + x;
      // the same point on the top and bottom half
      // bottom row:
      if (LED_ARRAY[array_pos] & LED_RED) SETPORT(LEDR2);
      else CLRPORT(LEDR2);
      if (LED_ARRAY[array_pos] & LED_GREEN) SETPORT(LEDG2);
      else CLRPORT(LEDG2);
      if (LED_ARRAY[array_pos] & LED_BLUE) SETPORT(LEDB2);
      else CLRPORT(LEDB2);
      // top row:
      if (LED_ARRAY[array_pos + array_sz / 2] & LED_RED) SETPORT(LEDR1);
      else CLRPORT(LEDR1);
      if (LED_ARRAY[array_pos + array_sz / 2] & LED_GREEN) SETPORT(LEDG1);
      else CLRPORT(LEDG1);
      if (LED_ARRAY[array_pos + array_sz / 2] & LED_BLUE) SETPORT(LEDB1);
      else CLRPORT(LEDB1);
      led_cycle_clk();
    }
    led_cycle_lat();

#ifdef ANTIGHOSTING
    led_cycle_oe(); // this combined with next delay is 0.1% PWM
    // force clear before starting new row
    for (int x = NUM_COLS; x >= 0; x--) {
      CLRPORT(LEDR1);
      CLRPORT(LEDG1);
      CLRPORT(LEDB1);
      CLRPORT(LEDR2);
      CLRPORT(LEDG2);
      CLRPORT(LEDB2);
      led_cycle_clk();
    }
    led_cycle_lat();
#endif

  }
}

void led_enable_autorun() {
  led_global_enable = 1;
}

// why make it if it's disabled by default? Maybe you re-disable during runtime.
void led_disable_autorun(void) {
  led_global_enable = 0;
}
