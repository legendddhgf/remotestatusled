#ifndef LEDLIB_H
#define LEDLIB_H

#include <avr/io.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

// general stuff

#define NUM_ROWS 16
#define NUM_COLS 32

// Pin layout expectations are as follows:

//pin 22
#define LEDR1_PORT PORTA
#define LEDR1_DDR DDRA
#define LEDR1_BITMSK _BV(0)
//pin 23
#define LEDG1_PORT PORTA
#define LEDG1_DDR DDRA
#define LEDG1_BITMSK _BV(1)
//pin 24
#define LEDB1_PORT PORTA
#define LEDB1_DDR DDRA
#define LEDB1_BITMSK _BV(2)
//pin 25
#define LEDR2_PORT PORTA
#define LEDR2_DDR DDRA
#define LEDR2_BITMSK _BV(3)
//pin 26
#define LEDG2_PORT PORTA
#define LEDG2_DDR DDRA
#define LEDG2_BITMSK _BV(4)
//pin 27
#define LEDB2_PORT PORTA
#define LEDB2_DDR DDRA
#define LEDB2_BITMSK _BV(5)
//pin 28
#define LEDA_PORT PORTA
#define LEDA_DDR DDRA
#define LEDA_BITMSK _BV(6)
//pin 29
#define LEDB_PORT PORTA
#define LEDB_DDR DDRA
#define LEDB_BITMSK _BV(7)
//pin 30
#define LEDC_PORT PORTC
#define LEDC_DDR DDRC
#define LEDC_BITMSK _BV(7)
//pin 31
#define LEDCLK_PORT PORTC
#define LEDCLK_DDR DDRC
#define LEDCLK_BITMSK _BV(6)
//pin 32
#define LEDLAT_PORT PORTC
#define LEDLAT_DDR DDRC
#define LEDLAT_BITMSK _BV(5)
//pin 33
#define LEDOE_PORT PORTC
#define LEDOE_DDR DDRC
#define LEDOE_BITMSK _BV(4)

// COLORS
#define LED_RED _BV(2)
#define LED_GREEN _BV(1)
#define LED_BLUE _BV(0)
#define LED_NOCOLOR 0

void led_init_pins(void);
void led_cycle_clk(void);
void led_cycle_lat(void);
void led_cycle_oe(void);
void led_cycle_oe_delay(double millis);
void led_set_color_for_pos(uint8_t x, uint8_t y, uint8_t rgb);
void led_add_color_for_pos(uint8_t x, uint8_t y, uint8_t rgb);
void led_draw_frame(void);
void led_enable_autorun(void);
void led_disable_autorun(void);

#define led_cycle_oe_delay(micros) {\
  SETPORT(LEDOE); \
  _delay_us(micros); \
  CLRPORT(LEDOE); \
}

#endif
